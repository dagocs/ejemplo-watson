# Ejemplo para la escuela de Otoño CICESE 2016

El presente ejemplo se realizó utilizando los distintos demos de los servicios de Watson: text-to-speech & speech-to-text.

El ejemplo presenta una interacción con la aplicación mediante lenguaje natural, y permite entablar un diálogo en base a un modelo desarrollado con el servicio Conversation Watson.
